<?php

/**
 * This function can be then called by cache module
 * and it will run watchdog entries through watchdog() function.
 */
function _cache_early_watchdog($type = FALSE) {
  static $watchdog = array();

  if ($type === FALSE) {
    if (count($watchdog) > 0) {
      foreach ($watchdog as $entry) {
        call_user_func_array('watchdog', $entry);
      }
      // Clear array.
      $watchdog = array();
    }
  }
  else {
    $watchdog[] = func_get_args();
  }
}


/**
 * Contributed from Drupal 7.x DrupalCacheInterface interface.
 */
interface DrupalCacheInterface {
  /**
   * Constructor.
   *
   * @param $bin
   *   The cache bin for which the object is created.
   */
  function __construct($bin);

  /**
   * Return data from the persistent cache. Data may be stored as either plain
   * text or as serialized data. cache_get will automatically return
   * unserialized objects and arrays.
   *
   * @param $cid
   *   The cache ID of the data to retrieve.
   * @return The cache or FALSE on failure.
   */
  function get($cid);

  /**
   * Return data from the persistent cache when given an array of cache IDs.
   *
   * @param $cids
   *   An array of cache IDs for the data to retrieve. This is passed by
   *   reference, and will have the IDs successfully returned from cache
   *   removed.
   * @return
   *   An array of the items successfully returned from cache indexed by cid.
   */
//   function getMultiple(&$cids);

  /**
   * Store data in the persistent cache.
   *
   * @param $cid
   *   The cache ID of the data to store.
   * @param $data
   *   The data to store in the cache. Complex data types will be automatically
   *   serialized before insertion.
   *   Strings will be stored as plain text and not serialized.
   * @param $expire
   *   One of the following values:
   *   - CACHE_PERMANENT: Indicates that the item should never be removed unless
   *     explicitly told to using cache_clear_all() with a cache ID.
   *   - CACHE_TEMPORARY: Indicates that the item should be removed at the next
   *     general cache wipe.
   *   - A Unix timestamp: Indicates that the item should be kept at least until
   *     the given time, after which it behaves like CACHE_TEMPORARY.
   * @param $headers
   *   A string containing HTTP header information for cached pages.
   */
  function set($cid, $data, $expire = CACHE_PERMANENT, array $headers = NULL);


  /**
   * Expire data from the cache. If called without arguments, expirable
   * entries will be cleared from the cache_page and cache_block bins.
   *
   * @param $cid
   *   If set, the cache ID to delete. Otherwise, all cache entries that can
   *   expire are deleted.
   * @param $wildcard
   *   If set to TRUE, the $cid is treated as a substring
   *   to match rather than a complete ID. The match is a right hand
   *   match. If '*' is given as $cid, the bin $bin will be emptied.
   */
  function clear($cid = NULL, $wildcard = FALSE);
}


/**
 * Base cache engine class.
 */
abstract class CacheEngine implements DrupalCacheInterface {
  /**
   * Caching engine settings.
   */
  var $settings = array();

  /**
   * Key/value pairs cached when $this->static is set to TRUE.
   */
  var $content = array();

  /**
   * Unique web-site prefix. Use for multisite environment.
   */
  var $prefix = '';

  /**
   * Name of bin (table), e.g. cache, cache_page, cache_form etc.
   */
  var $name = NULL;

  /**
   * Lookup key name. This key will contain array of all variables
   * set for current bin using this caching engine.
   */
  var $lookup = '';

  /**
   * Lock key name. Special key that is set to prevent concurrent
   * writes.
   */
  var $lock = '';

  /**
   * Whether page_fast_cache feature is turned on / off.
   */
  var $fast_cache = TRUE;

  /**
   * If TRUE then also cache fetched values in $this->content variable. Might
   * increase performance when enabled for cache_path_alias, cache_path_source
   * cache bins.
   */
  var $static = FALSE;

  function __construct($settings) {
    $this->settings = $settings;

    // Add defaults.
    $this->settings += array(
      'prefix' => '',
      'static' => FALSE,
      'fast_cache' => TRUE,
    );

    $this->prefix = $this->settings['prefix'];
    $this->name = $this->settings['bin'];
    $this->static = $this->settings['static'];
    $this->fast_cache = $this->settings['fast_cache'];

    // Setup prefixed lookup and lock table names for shared storage.
    $prefix = strlen($this->prefix) > 0 ? $this->prefix .',' : '';
    $this->lookup = $prefix .'lookup,'. $this->name;
    $this->lock = $prefix .'lock,'. $this->name;
  }

  function get($key) {
    if ($this->static && isset($this->content[$key])) {
      return $this->content[$key];
    }
  }

  function set($key, $value, $expire = CACHE_PERMANENT, array $headers = NULL) {
    if ($this->static) {
      $this->content[$key] = $value;
    }
  }

  function clear($key = NULL, $wildcard = FALSE) {
    if ($this->static) {
      if ($key == '*') {
        // Delete all caches.
        $this->content = array();
      }
      else if (substr($key, -1, 1) == '*') {
        $len = strlen($key) - 1;
        foreach ($this->content as $k => $v) {
          if (0 == strncmp($key, $k, $len)) {
            unset($this->content[$k]);
          }
        }
      }
      else {
        unset($this->content[$key]);
      }
    }
  }

  function flush() {
    if ($this->static) {
      $this->content = array();
    }
  }

  /**
   * Get the full key of the item.
   *
   * @param string $key
   *   The key to set.
   * @return string
   *   Returns the full key of the cache item.
   */
  function key($key) {
    $prefix = strlen($this->prefix) > 0 ? $this->prefix .',' : '';
    return urlencode($prefix . $this->name .','. $key);
  }

  /**
   * Implementation of getInfo().
   */
  static abstract function getInfo();

  /**
   * Cache engine requirements.
   */
  static function requirements() {
    // By default no requirements.
    return array();
  }

  /**
   * Statistics information.
   */
  function stats() {
    return NULL;
  }

}
