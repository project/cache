<p><em>Cache</em> module provides bridge between Drupal and alternative cache handler implementations like APC, memcache or redis. It provides implementation of <code>page_fast_cache()</code> function.</p>

<p>Installing <em>Cache</em> module is not enough to make alternative cache handlers work. You need to configure your web-site's <code>settings.php</code> configuration file also.</p>

<h3>Alternative caching handler</h3>

  <p>To enable alternative caching handler you might want to add to web-site's
  <code>settings.php</code>:</p>

  <pre>
    $conf['cache_inc'] = './sites/all/modules/cache/cache.inc';
    $conf['cache_settings'] = array(
      'engines' => array(
        'database-engine' => array(
          'engine' => 'database',
        ),
      ),
      'schemas' => array(
        'database-schema' => array(
          'database-engine',
        )
      ),
      'bins' => array(
        'default' => 'database-schema',
      ),
    );
  </pre>

  <p>"default" is for the default caching schema.  All valid cache bins (tables) can be added in addition, but you must have a "default" if you skip any bins.</p>

  <p>You can add several more cache schemas and use them for bins. Each schema is array of arrays, each array is contains configuration of engine. If several engines per schema is specified then cache chaining will be used: for example if cache entry is not found in 1st engine then we'll try to find it in 2nd, then in 3rd etc.</p>

  <p>This feature also requires core patch <code>path_inc-variable-support.patch</code> to be applied. It can be applied manually or applied through web administration using <a href="http://drupal.org/project/patchdoq">Patchdoq</a> module if installed at <em>Administer -> Site building -> Patch</em>.</p>

  <p>Cache bins <em>cache_path_alias</em> and <em>cache_path_source</em> are be created to store path aliases. You may configure any caching schema for them like for any other table. Aware that leaving it for default database engine might degrade performance.</p>

<h3>Supported alternative caching engines list</h3>

  @cache-engine-list

<h3>Alternative session handler</h3>

  <p>To enable alternative session handler you might want to add to web-site's
  <code>settings.php</code>:</p>

  <pre>
    $conf['session_inc'] = './sites/all/modules/cache/session.inc';
  </pre>

  <p>New bins <em>cache_session</em> and <em>cache_session_user</em> are created to store session information. You may configure any caching schema for them like for any other table.</p>

<h3>Alternative path alias handler</h3>

  <p>To enable alternative path storage handler you might want to add to web-site's <code>settings.php</code>:</p>

  <pre>
    $conf['path_inc'] = './sites/all/modules/cache/path/path-max.inc';
  </pre>

  <p>or</p>

  <pre>
    $conf['path_inc'] = './sites/all/modules/cache/path/path-cache.inc';
  </pre>


<h3>Addition information</h3>
  <p>
    For more information, see the online project page for <a href="http://drupal.org/project/cache">Cache module</a>.
  </p>
